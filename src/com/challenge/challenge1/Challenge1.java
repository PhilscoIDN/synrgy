package com.challenge.challenge1;

import java.util.Scanner;

public class Challenge1 {
    public static Scanner chooice = new Scanner(System.in);
    public static void main(String[] args) {
        menu();
    }

    private static void logic(){
        String bidangString="----------------------------\nPilih bidang yang akan dihitung\n----------------------------\n";
        String bidangLuas="1. persegi\n2. lingkaran\n3. segitiga\n4. persegi panjang\n0. kembali ke menu sebelumnya\n----------------------------";
        String bidangVolume="1. kubus\n2. balok\n3. tabung\n0. kembali ke menu sebelumnya\n----------------------------";
        String hasilLuas ="Luas dari bidang tersebut adalah : ";
        String hasilVolume ="Volume dari bidang tersebut adalah : ";

        try {
            Integer pilihan = chooice.nextInt();
            switch (pilihan) {
                case 1 -> {
                    System.out.println(bidangString + bidangLuas);
                    int bidang = chooice.nextInt();
                    switch (bidang) {
                        case 1-> {
                            System.out.print("Masukan sisi : ");
                            Integer aku = chooice.nextInt();
                            hitungLuasPersegi(hasilLuas,aku);
                            break;
                        }

                        case 2 -> {
                            System.out.print("Masukan jari jari : ");
                            Integer jari = chooice.nextInt();
                            System.out.println(hasilLuas+3.14 * (jari * jari));
                            break;
                        }

                        case 3 -> {
                            System.out.print("Masukan alas : ");
                            Integer alas = chooice.nextInt();
                            System.out.print("Masukan tinggi : ");
                            Integer tinggi = chooice.nextInt();
                            System.out.println(hasilLuas+0.5 * (alas * tinggi));
                            break;
                        }
                        case 4 -> {
                            System.out.print("Masukan panjang : ");
                            Integer p = chooice.nextInt();
                            System.out.print("Masukan lebar : ");
                            Integer l = chooice.nextInt();
                            System.out.println(hasilLuas+p * l);
                            break;
                        }

                        default -> {
                            menu();
                        }
                    }
                    break;
                }

                case 2 -> {
                    System.out.println(bidangString + bidangVolume);
                    int bidang = chooice.nextInt();
                    switch (bidang) {
                        case 1 -> {
                            System.out.print("Masukan Sisi Kubus : ");
                            Integer sisi = chooice.nextInt();
                            hitungVolumeKubus(hasilVolume,sisi);
                            break;
                        }

                        case 2 -> {
                            System.out.print("Masukan Panjang Balok : ");
                            Integer panjang = chooice.nextInt();
                            System.out.print("Masukan Lebar Balok : ");
                            Integer lebar = chooice.nextInt();
                            System.out.print("Masukan Tinggi Balok : ");
                            Integer tinggi = chooice.nextInt();
                            int volumeBalok = panjang*lebar*tinggi;
                            System.out.println(hasilVolume+volumeBalok);
                            break;
                        }

                        case 3 -> {
                            System.out.print("Masukan Tinggi Tabung : ");
                            Integer tinggi = chooice.nextInt();
                            System.out.print("Masukan jari -jari Balok : ");
                            Integer jari = chooice.nextInt();
                            double volumeTabung = 3.14 * tinggi*jari;
                            System.out.println(hasilVolume+volumeTabung);
                            break;
                        }

                        default -> {
                            break;
                        }
                    }
                    break;
                }

                default -> {
                    break;
                }
            }
        } finally {
            chooice.close();
        }
    }

    private static void hitungLuasPersegi(String c,Integer a){
        System.out.println(c+ a*a);
    }

    private static void hitungVolumeKubus(String b,Integer a){
        System.out.println(b+a*a*a);
    }

    private static void menu(){
        String menu ="\n\n----------------------------\nKalkulator Penghitung Luas dan Volume\n----------------------------\nMenu\n1. Hitung Luas Bidang\n2. Hitung Volume\n0. tutup Aplikasi\n----------------------------";
        System.out.println(menu);
        logic();
    }
}
